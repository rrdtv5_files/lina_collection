#include <iostream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

vector<vector<float>> matrix_multiplication(vector<vector<float>> matrix1, vector<vector<float>> matrix2){
	int dimension1_row = static_cast<int>(matrix1.size());
	int dimension1_column = static_cast<int>(matrix1[0].size());
	int dimension2_row = static_cast<int>(matrix2.size());
	int dimension2_column = static_cast<int>(matrix2[0].size());
	if(dimension1_column != dimension2_row){
		cout << "Error, matrix dimensions incompatible!" << endl;
		return {{0.0f}};
	}
	else{
		vector<vector<float>> result;
		for(int i=0; i<dimension1_row; i++){
			vector<float> row;
			for(int j=0; j<dimension2_column; j++){
				float temp = 0.0f;
				for(int k=0; k<dimension1_column; k++){
					temp += matrix1[i][k] * matrix2[k][j];
				}
			row.push_back(temp);
			}
		result.push_back(row);
		}
		return result;
	}
}

vector<vector<float>> matrix_addition(vector<vector<float>> matrix1, vector<vector<float>> matrix2){
	int dimension1_row = static_cast<int>(matrix1.size());
	int dimension1_column = static_cast<int>(matrix1[0].size());
	int dimension2_row = static_cast<int>(matrix2.size());
	int dimension2_column = static_cast<int>(matrix2[0].size());
	if((dimension1_column != dimension2_column) or (dimension1_row != dimension2_row)){
		cout << "Error, matrix dimensions incompatible!" << endl;
		return {{0.0f}};
	}
	else{
		vector<vector<float>> result;
		for(int i=0; i<dimension1_row; i++){
			vector<float> row;
			for(int j=0; j<dimension2_column; j++){
					float temp = matrix1[i][j] + matrix2[i][j];
			row.push_back(temp);
			}
		result.push_back(row);
		}
		return result;
	}
}

vector<vector<float>> matrix_scalar_multiplication(float scalar, vector<vector<float>> matrix){
	int dimension_row = static_cast<int>(matrix.size());
	int dimension_column = static_cast<int>(matrix[0].size());
	vector<vector<float>> result = matrix;
	for(int i=0; i<dimension_row; i++){
		for(int j=0; j<dimension_column; j++){
			result[i][j] = scalar * matrix[i][j];
		}
	}
	return result;
}

vector<vector<float>> matrix_scalar_division(float scalar, vector<vector<float>> matrix){
	float factor;
	try{
		factor = 1/scalar;
		if( factor != factor){
			throw(scalar);
		}
	}
	catch(float scalar){
		cout << "1/"<<scalar << "is yielding a NaN"<< endl; 
		return {{0.0f}};
	}
	return matrix_scalar_multiplication(factor, matrix);
}

void print_matrix(vector<vector<float>> matrix){
	int dimension_row = static_cast<int>(matrix.size());
	int dimension_column = static_cast<int>(matrix[0].size());
	for(int i=0; i<dimension_row;  i++){
		string row = " ";
		for(int j=0; j<dimension_column; j++){
			row = row + to_string(matrix[i][j]) + " ";
		}
		cout << row << endl;
	}
}



vector<vector<float>> identity_matrix(int dimension){
	vector<vector<float>> result;
	for(int i=0; i < dimension; i++){
		vector<float> row;
		float temp = 0.0f;
		for(int j=0; j < dimension; j++){
			if(i==j){
				temp = 1.0f;
			}
			else{
				temp = 0.0f;
			}
			row.push_back(temp);
		}
		result.push_back(row);
	}
	return result;
}

vector<vector<float>> matrix_power(int power, vector<vector<float>> matrix){
	vector<vector<float>> result = matrix;
	if(power == 0){
		return identity_matrix(matrix[0].size());
	}
	for(int i=0; i<(power-1); i++){
		result = matrix_multiplication(matrix, result);
	}
	return result;
}

int factorial(int n){
	if (n==0){
		return 1;
	}
	else{
		int result = 1;
		for (int i=1; i <=n; i++){
			result *= i;
		}
		return result;
	}
}

vector<vector<float>> taylor_expansion(int order, vector<vector<float>> matrix){
	vector<vector<float>> result = matrix_scalar_multiplication(0.0f, matrix);
	for(int i=0; i < order; i ++){
		vector<vector<float>> temp =  matrix_scalar_division(static_cast<float>(factorial(i)),matrix_power(i,matrix));
		result = matrix_addition(result,temp);
	}
	return result;
}

int main(){
	vector<vector<float>> matrix1 = {{0.0f,1.0f},{-1.0f,0.0f}};
	vector<vector<float>> matrix_exp = taylor_expansion(15, matrix1);
	print_matrix (matrix_exp);
	cout<< "Program completed"<< endl;
}
